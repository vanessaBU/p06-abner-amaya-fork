package com.amayaabner.game.utils;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

public class Constants {

    public static final String START_BUTTON_PATH = "start_button.png";

    public static final int APP_WIDTH = 800;
    public static final int APP_HEIGHT = 480;
    public static final float WORLD_TO_SCREEN = 32f;//1 meter in world = 32px on screen

    public static final Vector2 WORLD_GRAVITY = new Vector2(0, -10);

    public static final float GROUND_X = 0;
    public static final float GROUND_Y = 0;
    public static final float GROUND_WIDTH = 50f;
    public static final float GROUND_HEIGHT = 2f;
    public static final float GROUND_DENSITY = 0f;

    public static final float RUNNER_X = 2;
    public static final float RUNNER_Y = GROUND_Y + GROUND_HEIGHT;
    public static final float RUNNER_WIDTH = 1f;
    public static final float RUNNER_HEIGHT = 2f;
    public static final float RUNNER_GRAVITY_SCALE = 3f;
    public static float RUNNER_DENSITY = 0.5f;
    public static final float RUNNER_DODGE_X = 1.7f;
    public static final float RUNNER_DODGE_Y = 1.5f;
    public static final Vector2 RUNNER_JUMPING_LINEAR_IMPULSE = new Vector2(0, 13f);
    public static final float RUNNER_HIT_ANGULAR_IMPULSE = 10f;

    public static final float ENEMY_X = 25f;
    public static final float ENEMY_DENSITY = RUNNER_DENSITY;
    public static final float RUNNING_SHORT_ENEMY_Y = 1.5f;
    public static final float RUNNING_LONG_ENEMY_Y = 2f;
    public static final float FLYING_ENEMY_Y = 3f;
    public static final float ENEMY_STARTING_VELOCITY = -10f;
    public static final float ENEMY_VELOCITY_INCREMENT = -5f;

    public static final String BACKGROUND_IMAGE_PATH = "background.png";
    public static final String GROUND_IMAGE_PATH = "ground.png";
    public static final String RUNNER_WALK_ATLAS_PATH = "larger_walk_sheet.txt";
    public static final String RUNNER_WALK_REGION_NAME = "walk_sheet";
    public static final int RUNNER_WALK_FRAMES = 8;
    public static final float RUNNER_FRAME_DURATION = 0.05f;
    public static final String RUNNER_DEAD_ATLAS_PATH = "larger_dead_sheet.txt";//index 7 is used for dodge
    public static final String RUNNER_DEAD_REGION_NAME = "dead_sheet";
    public static final String RUNNER_JUMP_ATLAS_PATH = "larger_jump_sheet.txt";//index 3 is used for non-animated jump
    public static final String RUNNER_JUMP_REGION_NAME = "jump_sheet";

    public static final String[] RUNNING_SMALL_ENEMY_REGION_NAMES = new String[] {"ladyBug_walk1", "ladyBug_walk2"};
    public static final String[] RUNNING_LONG_ENEMY_REGION_NAMES = new String[] {"barnacle_bite1", "barnacle_bite2"};
    public static final String[] RUNNING_BIG_ENEMY_REGION_NAMES = new String[] {"spider_walk1", "spider_walk2"};
    public static final String[] RUNNING_WIDE_ENEMY_REGION_NAMES = new String[] {"worm_walk1", "worm_walk2"};
    public static final String[] FLYING_SMALL_ENEMY_REGION_NAMES = new String[] {"bee_fly1", "bee_fly2"};
    public static final String[] FLYING_WIDE_ENEMY_REGION_NAMES = new String[] {"fly_fly1", "fly_fly2"};
    public static final String CHARACTERS_ATLAS_PATH = "characters.txt";

    public static final int SCORE_INCREMENT = 10;
    public static final Color SCORE_FONT_COLOR = Color.RED;
    public static final float SCORE_X = 10;
    public static final float SCORE_Y = 460;
    public static final float SCORE_X_SCALE = 1.7f;
    public static final float SCORE_Y_SCALE = 1.7f;
    public static final String GAME_OVER_PATH = "game_over.png";
    public static final Color GAME_OVER_FONT_COLOR = Color.RED;
    //public static final float GAME_OVER_TEXT_X = 100;
    //public static final float GAME_OVER_TEXT_Y = 100;
    public static final float GAME_OVER_TEXT_X_SCALE = 1.7f;
    public static final float GAME_OVER_TEXT_Y_SCALE = 1.7f;
    public static final int SPEEDUP_INTERVAL = 5;

}
