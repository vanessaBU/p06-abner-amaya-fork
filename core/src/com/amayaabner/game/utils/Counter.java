package com.amayaabner.game.utils;

public class Counter {
    private int value;
    private int limit;
    public Counter(int limit){
        this.limit = limit;
        this.value = 0;
    }

    public Counter(int limit,int value){
        this.limit = limit;
        this.value = value;
    }

    public void increment(){
        value = (value + 1) % limit;
    }

    public int getValue(){
        return value;
    }

    public void setValue(int value){
        this.value = value;
    }

    public void resetLimit(int limit){
        this.limit = limit;
        this.value = 0;
    }

}
