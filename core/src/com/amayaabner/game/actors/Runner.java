package com.amayaabner.game.actors;

import com.amayaabner.game.box2d.RunnerUserData;
import com.amayaabner.game.utils.Constants;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;

public class Runner extends GameActor {

    private boolean dodging;
    private boolean jumping;
    private boolean hit;
    private Animation runningAnimation;
    private float stateTime;
    private TextureRegion jumpingTexture;
    private TextureRegion dodgingTexture;
    private TextureRegion hitTexture;

    public Runner(Body body) {
        super(body);
        TextureAtlas walkTextureAtlas = new TextureAtlas(Constants.RUNNER_WALK_ATLAS_PATH);
        TextureRegion[] runningFrames = new TextureRegion[Constants.RUNNER_WALK_FRAMES];
        for (int i = 1; i <= Constants.RUNNER_WALK_FRAMES; i++) {
            runningFrames[i-1] = walkTextureAtlas.findRegion(Constants.RUNNER_WALK_REGION_NAME,i);
        }
        runningAnimation = new Animation(Constants.RUNNER_FRAME_DURATION, runningFrames);
        stateTime = 0f;
        TextureAtlas deadTextureAtlas = new TextureAtlas(Constants.RUNNER_DEAD_ATLAS_PATH);
        TextureAtlas jumpTextureAtlas = new TextureAtlas(Constants.RUNNER_JUMP_ATLAS_PATH);
        jumpingTexture = jumpTextureAtlas.findRegion(Constants.RUNNER_JUMP_REGION_NAME,3);
        dodgingTexture = deadTextureAtlas.findRegion(Constants.RUNNER_DEAD_REGION_NAME,8);
        hitTexture = deadTextureAtlas.findRegion(Constants.RUNNER_DEAD_REGION_NAME,5);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);

        if (dodging) {
            batch.draw(dodgingTexture, screenRectangle.x, screenRectangle.y + screenRectangle.height / 4, screenRectangle.height,
                    screenRectangle.width*1.22f);
        } else if (hit) {
            // When he's hit we also want to apply rotation if the body has been rotated
            batch.draw(hitTexture, screenRectangle.x, screenRectangle.y, screenRectangle.width * 0.5f,
                    screenRectangle.height * 0.5f, screenRectangle.height, screenRectangle.width*1.22f, 1f, 1f,
                    (float) Math.toDegrees(body.getAngle()));
        } else if (jumping) {
            batch.draw(jumpingTexture, screenRectangle.x, screenRectangle.y, screenRectangle.width,
                    screenRectangle.height);
        } else {
            // Running
            stateTime += Gdx.graphics.getDeltaTime();
            batch.draw((TextureRegion) runningAnimation.getKeyFrame(stateTime, true), screenRectangle.x, screenRectangle.y,
                    screenRectangle.getWidth(), screenRectangle.getHeight());
        }
    }

    @Override
    public RunnerUserData getUserData() {
        return (RunnerUserData) userData;
    }

    public void jump() {

        if (!(jumping || dodging || hit)) {
            body.applyLinearImpulse(getUserData().getJumpingLinearImpulse(), body.getWorldCenter(), true);
            jumping = true;
        }
    }

    public void landed() {
        jumping = false;
    }

    public void dodge() {
        if (!(jumping || hit)) {
            body.setTransform(getUserData().getDodgePosition(), getUserData().getDodgeAngle());
            dodging = true;
        }
    }

    public void stopDodge() {
        dodging = false;
        if (!hit)
            body.setTransform(getUserData().getRunningPosition(), 0f);
    }

    public boolean isDodging() {
        return dodging;
    }

    public void hit() {
        body.applyAngularImpulse(getUserData().getHitAngularImpulse(), true);
        hit = true;
    }

    public boolean isHit(){
        return hit;
    }
}
