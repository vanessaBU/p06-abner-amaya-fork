package com.amayaabner.game.actors;

import com.amayaabner.game.stages.GameStage;
import com.amayaabner.game.utils.Constants;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class Score extends Actor {
    private BitmapFont font;

    public Score() {
        font = new BitmapFont();
        font.setColor(Constants.SCORE_FONT_COLOR);
        font.getData().setScale(Constants.SCORE_X_SCALE,Constants.SCORE_Y_SCALE);
    }

    @Override
    public void draw(Batch batch, float parentAlpha){
        font.draw(batch,"Score:" + GameStage.score, Constants.SCORE_X,Constants.SCORE_Y);
    }

}

