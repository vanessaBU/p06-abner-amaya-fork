package com.amayaabner.game.stages;

import com.amayaabner.game.actors.Background;
import com.amayaabner.game.actors.Enemy;
import com.amayaabner.game.actors.GameOverText;
import com.amayaabner.game.actors.Ground;
import com.amayaabner.game.actors.Runner;
import com.amayaabner.game.actors.Score;
import com.amayaabner.game.utils.BodyUtils;
import com.amayaabner.game.utils.Constants;
import com.amayaabner.game.utils.Counter;
import com.amayaabner.game.utils.WorldUtils;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;

import java.util.ArrayList;


public class GameStage extends Stage implements ContactListener {

    // viewport measurements while working with debug renderer
    private static final int VIEWPORT_WIDTH = Constants.APP_WIDTH;
    private static final int VIEWPORT_HEIGHT = Constants.APP_HEIGHT;

    private World world;
    private Ground ground;
    private Runner runner;
    private ArrayList<Background> backgroundLayers;

    private final float TIME_STEP = 1 / 300f;
    private float accumulator = 0f;

    private OrthographicCamera camera;

    private Rectangle screenLeftSide;
    private Rectangle screenRightSide;

    private Vector3 touchPoint;
    private Texture gameOverTexture;
    private TextureRegion texRegion;
    private TextureRegionDrawable texDrawable;
    private ImageButton gameOverButton;
    private BitmapFont font;

    public static int score;
    public boolean gameOver = false;
    public boolean shouldRestart = false;
    public Vector2 enemyVelocity;
    public Counter enemyCounter;

    public static Preferences prefs;

    public GameStage() {
        super(new ScalingViewport(Scaling.stretch, VIEWPORT_WIDTH, VIEWPORT_HEIGHT,
                new OrthographicCamera(VIEWPORT_WIDTH, VIEWPORT_HEIGHT)));
        score = 0;
        enemyCounter = new Counter(Constants.SPEEDUP_INTERVAL,Constants.SPEEDUP_INTERVAL-1);
        enemyVelocity = new Vector2(Constants.ENEMY_STARTING_VELOCITY,0);
        backgroundLayers = new ArrayList<Background>();
        setUpWorld();
        setupCamera();
        setupTouchControlAreas();
        addActor(new Score());

        // create new or retrieve existing preferences file
        prefs = Gdx.app.getPreferences("Ryvan");
        // provide default high score of 0
        if (!prefs.contains("highScore")) {
            prefs.putInteger("highScore", 0);
        }
    }

    private void setUpWorld() {
        world = WorldUtils.createWorld();
        world.setContactListener(this);

        setUpBackground();
        setUpGround();
        setUpRunner();
        createEnemy();
    }

    private void setUpBackground(){
        backgroundLayers.add(new Background(20,10,"sky_layer.png"));
        backgroundLayers.add(new Background(60,30,"cloud_layer.png"));
        backgroundLayers.add(new Background(90,45,"mountain_layer.png"));
        for (Background layer : backgroundLayers){
            addActor(layer);
        }
    }

    private void setUpGround() {
        ground = new Ground(WorldUtils.createGround(world));
        addActor(ground);
    }

    private void setUpRunner() {
        runner = new Runner(WorldUtils.createRunner(world));
        addActor(runner);
    }

    private void setupCamera() {

        camera = new OrthographicCamera(VIEWPORT_WIDTH, VIEWPORT_HEIGHT);
        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0f);
        camera.update();
    }

    private void setupTouchControlAreas() {
        touchPoint = new Vector3();
        screenLeftSide = new Rectangle(0, 0, getCamera().viewportWidth / 2, getCamera().viewportHeight);
        screenRightSide = new Rectangle(getCamera().viewportWidth / 2, 0, getCamera().viewportWidth / 2,
                getCamera().viewportHeight);
        Gdx.input.setInputProcessor(this);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        Array<Body> bodies = new Array<Body>(world.getBodyCount());
        world.getBodies(bodies);

        for (Body body : bodies) {
            update(body);
        }

        accumulator += delta; // fixed time-step

        while (accumulator >= delta) {
            world.step(TIME_STEP, 6, 2);
            accumulator -= TIME_STEP;
        }
    }

    private void update(Body body) {
        if (!BodyUtils.bodyInBounds(body)) {
            if (BodyUtils.bodyIsEnemy(body) && !runner.isHit()) {
                createEnemy();
                GameStage.score += Constants.SCORE_INCREMENT;
                if (GameStage.score > getHighScore()) {
                    setHighScore(GameStage.score);
                }
            }
            world.destroyBody(body);
        }
    }

    private void createEnemy() {
        Gdx.app.debug("EnemyVelocity", enemyVelocity.toString());
        Enemy enemy = new Enemy(WorldUtils.createEnemy(world));

        enemy.getUserData().setLinearVelocity(enemyVelocity);
        addActor(enemy);
        enemyCounter.increment();
        if (enemyCounter.getValue() == 0){
            enemyVelocity = new Vector2(enemyVelocity.x+Constants.ENEMY_VELOCITY_INCREMENT, 0);
            for (Background b : backgroundLayers){
                b.speedUp();
            }
        }
    }

    @Override
    public boolean touchDown(int x, int y, int pointer, int button) {

        // need to get the actual coordinates
        translateScreenToWorldCoordinates(x, y);

        if (rightSideTouched(touchPoint.x, touchPoint.y)) {
            runner.jump();
        }
        else if (leftSideTouched(touchPoint.x, touchPoint.y)) {
            runner.dodge();
        }
        if (gameOver){
            shouldRestart = true;
        }
        return super.touchDown(x, y, pointer, button);
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {

        if (runner.isDodging()) {
            runner.stopDodge();
        }

        return super.touchUp(screenX, screenY, pointer, button);
    }

    private boolean rightSideTouched(float x, float y) {
        return screenRightSide.contains(x, y);
    }

    private boolean leftSideTouched(float x, float y) {
        return screenLeftSide.contains(x, y);
    }

    /**
     * Helper function to get the actual coordinates in my world
     * @param x
     * @param y
     */
    private void translateScreenToWorldCoordinates(int x, int y) {
        getCamera().unproject(touchPoint.set(x, y, 0));
    }

    @Override
    public void beginContact(Contact contact) {

        Body a = contact.getFixtureA().getBody();
        Body b = contact.getFixtureB().getBody();

        if ((BodyUtils.bodyIsRunner(a) && BodyUtils.bodyIsEnemy(b)) ||
                (BodyUtils.bodyIsEnemy(a) && BodyUtils.bodyIsRunner(b))) {
            runner.hit();
            addGameOverImage();
            gameOver = true;
        } else if ((BodyUtils.bodyIsRunner(a) && BodyUtils.bodyIsGround(b)) ||
                (BodyUtils.bodyIsGround(a) && BodyUtils.bodyIsRunner(b))) {
            runner.landed();
        }

    }

    private void addGameOverImage() {
        gameOverTexture = new Texture(Constants.GAME_OVER_PATH);
        texRegion = new TextureRegion(gameOverTexture);
        texDrawable = new TextureRegionDrawable(texRegion);
        gameOverButton = new ImageButton(texDrawable);
        gameOverButton.setPosition(getCamera().viewportWidth / 4, getCamera().viewportHeight / 4);
        addActor(gameOverButton);
        //addActor(new GameOverText(score,0));
        addActor(new GameOverText(score, getHighScore()));
    }

    // receives integer and maps it to the String highScore in prefs
    public static void setHighScore(int val) {
        prefs.putInteger("highScore", val);
        prefs.flush();
    }

    // retrieves current high score
    public static int getHighScore() {
        return prefs.getInteger("highScore");
    }

    @Override
    public void endContact(Contact contact) {

    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}
