package com.amayaabner.game.enums;

public enum UserDataType {

    GROUND,
    RUNNER,
    ENEMY
}
