package com.amayaabner.game.screens;

import com.amayaabner.game.Ryvan;
import com.amayaabner.game.stages.GameStage;
import com.amayaabner.game.utils.Constants;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.physics.box2d.ContactListener;

public class GameScreen implements Screen{

    private GameStage stage;
    private Ryvan game;

    public GameScreen(final Ryvan game) {

        stage = new GameStage();
        this.game = game;
    }

    @Override
    public void render(float delta) {
        // clear screen
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        // update stage
        if (stage.gameOver && stage.shouldRestart){
            if (stage.gameOver){
                game.setScreen(new GameScreen(game));
            }
        }
        stage.draw();
        stage.act(delta);

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
