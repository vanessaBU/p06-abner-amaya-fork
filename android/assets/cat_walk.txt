cat_walk.png
size: 140, 30
format: RGBA8888
filter: Linear,Linear
repeat: none
walk_sheet
  rotate: false
  xy: 0, 0
  size: 18, 29
  orig: 18, 29
  offset: 0, 0
  index: 1
walk_sheet
  rotate: false
  xy: 18, 0
  size: 18, 29
  orig: 18, 29
  offset: 0, 0
  index: 2
walk_sheet
  rotate: false
  xy: 36, 0
  size: 17, 30
  orig: 17, 30
  offset: 0, 0
  index: 3
walk_sheet
  rotate: false
  xy: 53, 0
  size: 17, 30
  orig: 17, 30
  offset: 0, 0
  index: 4
walk_sheet
  rotate: false
  xy: 70, 0
  size: 18, 29
  orig: 18, 29
  offset: 0, 0
  index: 5
walk_sheet
  rotate: false
  xy: 88, 0
  size: 17, 29
  orig: 17, 29
  offset: 0, 0
  index: 6
walk_sheet
  rotate: false
  xy: 105, 0
  size: 17, 30
  orig: 17, 30
  offset: 0, 0
  index: 7
walk_sheet
  rotate: false
  xy: 122, 0
  size: 18, 29
  orig: 18, 29
  offset: 0, 0
  index: 8
